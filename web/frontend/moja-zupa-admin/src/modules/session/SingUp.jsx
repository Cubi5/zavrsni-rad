import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { withFirebase } from "../firebase";

function SingUp(props) {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [repeatedPassword, setRepeatedPassword] = useState("");
  const [error, setError] = useState("");

  const changeName = (event) => {
    setName(event.target.value);
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  const changeRepeatedPassword = (event) => {
    setRepeatedPassword(event.target.value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    props.firebase
      .createUserWithEmailAndPassword(name, password)
      .then(() => {
        props.history.push("/home");
      })
      .catch((err) => {
        // TODO Handle error properly
        setError(err.message);
        console.log(err);
      });
  };

  const isInvalid =
    password !== repeatedPassword ||
    password.length < 8 ||
    name.indexOf("@") === -1;

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>Registacija novog korisnika</h1>
      </div>
      <hr />
      <div className="panel-body">
        <form className="login-form" onSubmit={onSubmit}>
          <label className="login-label">
            <h3>E-mail</h3>
            <input
              type="text"
              value={name}
              onChange={changeName}
              placeholder="Ime"
              className="panel-input"
            ></input>
          </label>
          <label className="login-label">
            <h3>Lozinka</h3>
            <input
              type="password"
              value={password}
              onChange={changePassword}
              placeholder="Lozinka"
              className="panel-input"
            ></input>
          </label>
          <label className="login-label">
            <h3>Ponovite lozinku</h3>
            <input
              type="password"
              value={repeatedPassword}
              onChange={changeRepeatedPassword}
              placeholder="Lozinka"
              className="panel-input"
            ></input>
          </label>
          {error === "" ? "" : <p className="error">{error}</p>}
          <div className="login-button-container">
            <button
              className="panel-button"
              onClick={onSubmit}
              disabled={isInvalid}
            >
              Registriraj novog korisnika
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

const SignUpPage = withRouter(withFirebase(SingUp));

export { SignUpPage };
