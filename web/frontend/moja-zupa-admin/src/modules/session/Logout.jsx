import React from "react";
import { withRouter } from "react-router";
import { withFirebase } from "../firebase";

const Logout = (props) => {
  return (
    <button
      className="sign-out-button"
      onClick={() => {
        props.firebase.signOut();
        props.history.push("/login");
      }}
    >
      Odjavi se
    </button>
  );
};

export default withRouter(withFirebase(Logout));
