import React, { useState } from "react";
import { withFirebase } from "../firebase";
import { withRouter } from "react-router-dom";
import "../../styles/login.css";
import "../../styles/category.css";

function LoginScreenBase(props) {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const changeName = (event) => {
    setName(event.target.value);
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    props.firebase
      .signInWithEmailAndPassword(name, password)
      .then(() => {
        props.history.push("/home");
      })
      .catch((err) => {
        // TODO Handle error properly
        setError(err.message);
        console.log(err);
      });
  };

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>Prijava</h1>
      </div>
      <hr />
      <div className="panel-body">
        <form className="login-form" onSubmit={onSubmit}>
          <label className="login-label">
            <h3>E-mail</h3>
            <input
              type="text"
              value={name}
              onChange={changeName}
              placeholder="Ime"
              className="panel-input"
            ></input>
          </label>
          <label className="login-label">
            <h3>Lozinka</h3>
            <input
              type="password"
              value={password}
              onChange={changePassword}
              placeholder="Lozinka"
              className="panel-input"
            ></input>
          </label>
          {error === "" ? "" : <p className="error">{error}</p>}
          <div className="login-button-container">
            <button className="panel-button" onClick={onSubmit}>
              Prijavi se
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

const LoginScreen = withRouter(withFirebase(LoginScreenBase));

export { LoginScreen };
