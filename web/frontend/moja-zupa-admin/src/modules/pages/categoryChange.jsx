import React, { useState } from "react";
import { withAuthorization } from "../session";
import "../../styles/category.css";

const CategoryChange = (props) => {
  const [name, setName] = useState(props.category.name);
  const [informacije, setInformacije] = useState(props.category.informacije);

  const changeName = (e) => {
    setName(e.target.value);
  };

  const changeInformation = (e, index) => {
    const infos = [...informacije];
    infos[index] = e.target.value;
    setInformacije(infos);
  };

  const deleteInformation = (e, index) => {
    const infos = informacije.filter((v, i) => i !== index);
    setInformacije(infos);
  };

  const addInformation = () => {
    const infos = [...informacije, ""];
    setInformacije(infos);
  };

  const moveInformation = (index, n) => {
    if (index + n >= 0 && index + n < informacije.length) {
      var info = null;
      var infos = informacije.filter((item, i) => {
        if (i === index) {
          info = item;
          return false;
        } else {
          return true;
        }
      });
      infos.splice(index + n, 0, info);
      setInformacije(infos);
    }
  };

  const saveData = (data) => {
    const topic =
      props.category.topic === null ? toASCII(name) : props.category.topic;
    props.onSave({ name, topic, informacije });
  };

  const toASCII = (str) => {
    var res = "";
    for (var s of str) {
      if (
        (s.charCodeAt() >= "A".charCodeAt() &&
          s.charCodeAt() <= "Z".charCodeAt()) ||
        (s.charCodeAt() >= "a".charCodeAt() &&
          s.charCodeAt() <= "z".charCodeAt()) ||
        (s.charCodeAt() >= "0".charCodeAt() &&
          s.charCodeAt() <= "9".charCodeAt()) ||
        s === "-" ||
        s === "." ||
        s === "_" ||
        s === "~" ||
        s === "%"
      ) {
        res += s;
      } else {
        res += "-";
      }
    }

    return res;
  };

  return (
    <div className="panel-changes">
      <h2 className="panel-title">Promijeni podatke o odabranoj stavci</h2>
      <label className="panel-changes-child">
        <div className="panel-changes-attribute">Ime:</div>
        <input
          type="text"
          name="name"
          value={name}
          onChange={changeName}
          className="panel-input"
        ></input>
      </label>
      <div className="panel-changes-child">
        <div className="panel-changes-informations">
          Informacije:
          {informacije.map((info, key) => {
            console.log(info, key);
            return (
              <div key={key} className="panel-changes-information">
                <textarea
                  type="text"
                  value={informacije[key]}
                  onChange={(e) => changeInformation(e, key)}
                  className="panel-textarea"
                  maxLength={500}
                ></textarea>
                <button
                  className="panel-button"
                  onClick={(e) => deleteInformation(e, key)}
                >
                  Obriši
                </button>
                <button
                  className="panel-button"
                  onClick={() => moveInformation(key, 1)}
                >
                  Pomakni dolje
                </button>
                <button
                  className="panel-button"
                  onClick={() => moveInformation(key, -1)}
                >
                  Pomakni gore
                </button>
              </div>
            );
          })}
        </div>
        <button className="panel-button" onClick={addInformation}>
          Dodaj
        </button>
      </div>
      <hr />
      <div className="panel-buttons">
        <button className="panel-button" onClick={saveData}>
          Spremi promjene
        </button>
        <button className="panel-button" onClick={props.onCancel}>
          Odustani
        </button>
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const CategoryChangeDialog = withAuthorization(cond)(CategoryChange);

export default CategoryChangeDialog;
