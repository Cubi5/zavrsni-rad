import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { withAuthorization } from "../session";
import "../../styles/category.css";
import "../../styles/hodocasnici.css";

const Hodocasnici = (props) => {
  const [hodocasnici, setHodocasnici] = useState([]);
  const { name } = useParams();

  useEffect(() => {
    setHodocasnici([]);
    props.firebase
      .getCollection("Hodočašća/" + name + "/Hodocasnici")
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          console.log(doc.id);
          setHodocasnici((hodocasnici) => [
            ...hodocasnici,
            { ...doc.data(), id: doc.id },
          ]);
        });
      });
  }, [name, props.firebase]);

  const deleteHodocasnik = (hodocasnik) => {
    props.firebase.deleteDocumentFromCollection(
      "Hodočašća/" + name + "/Hodocasnici",
      hodocasnik.id
    );
    setHodocasnici(hodocasnici.filter((h) => h !== hodocasnik));
  };

  const onSave = (data) => {
    props.firebase.db
      .collection("Hodočašća/" + name + "/Hodocasnici")
      .add(data)
      .then((res) => {
        setHodocasnici([...hodocasnici, { ...data, id: res.id }]);
      });
  };

  const compare = (a, b) => {
    if (a.lastname > b.lastname) {
      return 1;
    } else if (a.lastname < b.lastname) {
      return -1;
    } else {
      if (a.firstname > b.firstname) {
        return 1;
      } else if (a.firstname < b.firstname) {
        return -1;
      }
    }

    return 0;
  };

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>Hodočasnici - {name}</h1>
      </div>
      <hr />
      <div className="panel-body">
        <div className="hodocasnici-list">
          <div className="hodocasnik">
            <div className="hodocasnik-field hodocasnik-head">Ime</div>
            <div className="hodocasnik-field hodocasnik-head">Prezime</div>
            <div className="hodocasnik-field hodocasnik-head">Ime oca</div>
            <div className="hodocasnik-field hodocasnik-head">Plaćeno</div>
            <div className="hodocasnik-field hodocasnik-head"></div>
          </div>
          <hr />
          {hodocasnici.sort(compare).map((hod) => {
            return (
              <div key={hod.id} className="hodocasnik">
                <div className="hodocasnik-field">{hod.firstname}</div>
                <div className="hodocasnik-field">{hod.lastname}</div>
                <div className="hodocasnik-field">{hod.father}</div>
                <div className="hodocasnik-field">
                  {hod.payed ? "Da" : "Ne"}
                </div>
                <div className="hodocasnik-field">
                  <button
                    className="panel-button"
                    onClick={() => deleteHodocasnik(hod)}
                  >
                    Obriši
                  </button>
                </div>
              </div>
            );
          })}
          <hr />
          <HodocasnikForm onSave={onSave}></HodocasnikForm>
        </div>
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const HodocasniciPage = withAuthorization(cond)(Hodocasnici);

export default HodocasniciPage;

const HodocasnikForm = (props) => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [father, setFather] = useState("");
  const [payed, setPayed] = useState(false);

  const save = () => {
    props.onSave({ firstname, lastname, father, payed });
    setFirstname("");
    setLastname("");
    setFather("");
    setPayed(false);
  };

  return (
    <>
      <div className="hodocasnik">
        <div className="hodocasnik-field">Dodajte novog hodočasnika</div>
      </div>
      <div className="hodocasnik">
        <div className="hodocasnik-field">
          <input
            className="hodocasnik-input"
            type="text"
            placeholder="Ime"
            value={firstname}
            onChange={(e) => {
              e.preventDefault();
              setFirstname(e.target.value);
            }}
          />
        </div>
        <div className="hodocasnik-field">
          <input
            className="hodocasnik-input"
            type="text"
            placeholder="Prezime"
            value={lastname}
            onChange={(e) => {
              e.preventDefault();
              setLastname(e.target.value);
            }}
          />
        </div>
        <div className="hodocasnik-field">
          <input
            className="hodocasnik-input"
            type="text"
            placeholder="Ime oca"
            value={father}
            onChange={(e) => {
              e.preventDefault();
              setFather(e.target.value);
            }}
          />
        </div>
        <div className="hodocasnik-field">
          <button
            className={payed ? "radio-button-selected" : "radio-button"}
            onClick={() => setPayed(true)}
          >
            Da
          </button>
          <button
            className={!payed ? "radio-button-selected" : "radio-button"}
            onClick={() => setPayed(false)}
          >
            Ne
          </button>
        </div>
        <div className="hodocasnik-field">
          <button className="panel-button" onClick={save}>
            Dodaj
          </button>
        </div>
      </div>
    </>
  );
};
