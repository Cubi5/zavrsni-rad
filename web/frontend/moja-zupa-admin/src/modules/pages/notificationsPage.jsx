import React, { useEffect, useState } from "react";
import { withAuthorization } from "../session";
import { useParams } from "react-router";
import NotificationForm from "./notificationForm";
import "../../styles/category.css";
import "../../styles/notifications.css";

const Notifications = (props) => {
  const [notifications, setNotifications] = useState([]);
  const [add, setAdd] = useState(false);
  const [keys, setKeys] = useState([]);
  const { subCategory } = useParams();

  useEffect(() => {
    setNotifications([]);
    props.firebase
      .getCollection(
        props.collection + "/" + subCategory + "/" + props.collection
      )
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          console.log(doc.data());
          setNotifications((notifications) => [...notifications, doc.data()]);
        });
      });
  }, [props.collection, props.firebase, subCategory]);

  const removeNotification = (notification) => {
    setNotifications(notifications.filter((n) => n !== notification));
    props.firebase.deleteDocumentFromCollection(
      props.collection + "/" + subCategory + "/" + props.collection,
      notification.name
    );
  };

  const onSave = (data) => {
    const filledData = {
      ...data,
      name: `${data.title} - ${new Date()}`,
      timestamp: props.firebase.getFirebaseTimestamp(new Date()),
    };
    props.firebase.addToCollection(
      props.collection + "/" + subCategory + "/" + props.collection,
      filledData
    );
    setAdd(false);
    setNotifications((notifications) => [...notifications, filledData]);
  };

  const onCancel = () => {
    setAdd(false);
  };

  const getNotifications = () => {
    if (notifications.length === 0) {
      return <h3>Još nema obavijesti!</h3>;
    }
    return notifications
      .sort((a, b) => b.timestamp - a.timestamp)
      .map((notification, key) => {
        const date = notification.timestamp.toDate();
        return (
          <div
            key={notification.title + " " + notification.timestamp}
            className="notification-list"
          >
            {keys.indexOf(key) === -1 ? (
              <>
                <div className="notification-right">
                  <div className="notification-top">
                    <div>{notification.title}</div>
                    <div>
                      Datum:{" "}
                      {`${date.getDate()}.${
                        date.getMonth() + 1
                      }.${date.getFullYear()}`}
                    </div>
                  </div>
                  <hr />
                  <div className="notification-bottom">
                    {notification.content}
                  </div>
                </div>
                <button
                  className="panel-button"
                  onClick={() => setKeys([...keys, key])}
                >
                  Promijeni
                </button>
                <button
                  className="panel-button"
                  onClick={() => {
                    removeNotification(notification);
                    setKeys(keys.filter((k) => k !== key));
                  }}
                >
                  Obriši
                </button>
              </>
            ) : (
              <NotificationForm
                onSave={(data) => {
                  removeNotification(notification);
                  setKeys(keys.filter((k) => k !== key));
                  onSave(data);
                }}
                onCancel={() => {
                  setKeys(keys.filter((k) => k !== key));
                }}
                title={notification.title}
                content={notification.content}
              ></NotificationForm>
            )}
          </div>
        );
      });
  };

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>
          {props.title} - {subCategory}
        </h1>
      </div>
      <hr />
      <div className="panel-body">
        <div className="notification-body">
          {getNotifications()}
          {add ? (
            <NotificationForm
              onSave={onSave}
              onCancel={onCancel}
            ></NotificationForm>
          ) : (
            <button className="panel-button" onClick={() => setAdd(true)}>
              Dodaj obavijest
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const NotificationsPage = withAuthorization(cond)(Notifications);

export default NotificationsPage;
