import React, { useState } from "react";
import { withAuthorization } from "../session";
import "../../styles/category.css";
import "../../styles/notifications.css";

const Form = (props) => {
  const [title, setTitle] = useState(!!props.title ? props.title : "");
  const [content, setContent] = useState(!!props.content ? props.content : "");

  return (
    <div className="notification-form">
      <div className="notification-input">
        <h3>Naslov obavijesti</h3>
        <input
          type="text"
          name="name"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          className="panel-input"
        ></input>
      </div>
      <div className="notification-textarea-container">
        <h3>Sadržaj obavijesti</h3>
        <textarea
          type="text"
          value={content}
          onChange={(e) => setContent(e.target.value)}
          className="notification-textarea"
        ></textarea>
      </div>
      <div className="panel-buttons">
        <button
          className="panel-button"
          onClick={() => props.onSave({ title, content })}
        >
          Spremi promjene
        </button>
        <button className="panel-button" onClick={props.onCancel}>
          Odustani
        </button>
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const NotificationForm = withAuthorization(cond)(Form);

export default NotificationForm;
