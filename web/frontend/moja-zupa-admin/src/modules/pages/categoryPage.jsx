import React, { useEffect, useState } from "react";
import { withAuthorization } from "../session";
import CategoryChangeDialog from "./categoryChange";
import "../../styles/category.css";

const emptyCategory = { name: "", topic: null, informacije: [] };

const Category = (props) => {
  const [categorys, setCategory] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(emptyCategory);
  const [isChanging, setIsChanging] = useState(false);

  useEffect(() => {
    setCategory([]);
    setSelectedCategory(emptyCategory);
    props.firebase.getCollection(props.collection).then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        setCategory((sections) => [...sections, doc.data()]);
      });
    });
  }, [props.collection, props.firebase]);

  const onSectionClick = (category) => {
    setSelectedCategory(category);
  };

  const getSectionNames = () => {
    return categorys.map((category, i) => (
      <div
        key={category.name + i}
        className={
          "panel-section-title-container " +
          (category === selectedCategory
            ? " panel-section-container-selected"
            : "bottom-border")
        }
      >
        <h2
          className={
            "panel-section-title" +
            (category === selectedCategory ? " panel-section-selected" : "")
          }
          onClick={() => onSectionClick(category)}
        >
          {category.name}
        </h2>
        <div>
          {props.collection === "Hodočašća" && (
            <button
              className="panel-button"
              onClick={() => props.history.push(`/hodocasce/${category.name}`)}
            >
              Hodočasnici
            </button>
          )}

          <button
            className="panel-button"
            onClick={() => deleteSection(category.name)}
          >
            Obriši
          </button>
        </div>
      </div>
    ));
  };

  const getInformations = () => {
    return selectedCategory.informacije.map((info, i) => {
      return (
        <li key={i} className="panel-information">
          {info}
        </li>
      );
    });
  };

  const deleteSection = (name) => {
    props.firebase.deleteDocumentFromCollection(props.collection, name);
    props.firebase.deleteDocumentFromCollection("Obavijesti", name);
    setCategory(categorys.filter((c) => c.name !== name));
  };

  const saveChanges = (data) => {
    props.firebase.addToCollection(props.collection, data);
    props.firebase.addToCollection("Obavijesti", {
      category: props.collection,
      name: data.name,
      topic: data.topic,
    });
    setCategory((sections) => [...sections, data]);
    setSelectedCategory(data);
    setIsChanging(false);
  };

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>{props.title}</h1>
      </div>
      <hr />
      <div className="panel-body">
        {!isChanging ? (
          <>
            <div className="panel-body-left">
              {getSectionNames()}
              <button
                className="panel-button"
                onClick={() => {
                  setSelectedCategory(emptyCategory);
                  setIsChanging(true);
                }}
              >
                Dodaj novu podkategoriju
              </button>
            </div>
            <div className="panel-body-right">
              {selectedCategory !== emptyCategory ? (
                <>
                  <div className="panel-attributes">
                    Ime: {selectedCategory !== null && selectedCategory.name}
                  </div>
                  <div className="panel-attributes">
                    <div>Informacije:</div>
                    <ul>
                      {selectedCategory === null ? "" : getInformations()}
                    </ul>
                  </div>
                  <button
                    className="panel-button"
                    onClick={() => setIsChanging(true)}
                    disabled={selectedCategory === emptyCategory}
                  >
                    Promijeni
                  </button>{" "}
                </>
              ) : (
                <div className="panel-attributes">
                  Odaberite potkategoriju za prikaz informacija
                </div>
              )}
            </div>
          </>
        ) : (
          <CategoryChangeDialog
            collection={props.collection}
            onSave={(data) => {
              setCategory(categorys.filter((c) => c !== selectedCategory));
              setSelectedCategory(emptyCategory);
              saveChanges(data);
            }}
            onCancel={() => setIsChanging(false)}
            category={selectedCategory}
          ></CategoryChangeDialog>
        )}
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const CategoryPage = withAuthorization(cond)(Category);

export default CategoryPage;
