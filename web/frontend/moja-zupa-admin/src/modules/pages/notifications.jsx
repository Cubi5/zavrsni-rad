import React, { useEffect, useState } from "react";
import { withAuthorization } from "../session";
import "../../styles/category.css";

const Notification = (props) => {
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("Općenito");

  useEffect(() => {
    setCategories([]);

    props.firebase.getCollection(props.collection).then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        setSubCategories((subCategories) => [...subCategories, doc.data()]);
        setCategories((categories) => {
          if (categories.indexOf(doc.data()["category"]) === -1) {
            return [...categories, doc.data()["category"]];
          } else {
            return [...categories];
          }
        });
      });
    });
  }, [props.collection, props.firebase]);

  const onCategoryClick = (category) => {
    setSelectedCategory(category);
  };

  const getCategoriesRender = () => {
    return categories.map((category, i) => (
      <div
        key={category.name + i}
        className={
          "panel-section-title-container " +
          (category === selectedCategory
            ? " panel-section-container-selected"
            : "bottom-border")
        }
      >
        <h2
          className={
            "panel-section-title" +
            (category === selectedCategory ? " panel-section-selected" : "")
          }
          onClick={() => onCategoryClick(category)}
        >
          {category}
        </h2>
      </div>
    ));
  };

  const onSubCategoryClick = (subCategory) => {
    props.history.push(`/obavijesti/${subCategory.name}`);
  };

  const getSubCategoriesRender = () => {
    return subCategories
      .filter((v) => v.category === selectedCategory)
      .map((subCategory, i) => (
        <div key={subCategory.name} className="panel-section-title-container">
          <h2
            className="panel-section-title"
            onClick={() => onSubCategoryClick(subCategory)}
          >
            {subCategory.name}
          </h2>
        </div>
      ));
  };

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>{props.title}</h1>
      </div>
      <hr />
      <div className="panel-body">
        <div className="panel-body-left">{getCategoriesRender()}</div>
        <div className="panel-body-left left-border">
          {getSubCategoriesRender()}
        </div>
      </div>
    </div>
  );
};

const cond = (user) => !!user;

const Notifications = withAuthorization(cond)(Notification);

export default Notifications;
