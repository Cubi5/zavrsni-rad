import React, { useEffect, useState } from "react";
import photo from "../../images/Crkva_na_Buni.png";
import { withRouter } from "react-router";
import { withFirebase } from "../firebase";
import "../../styles/category.css";
import "../../styles/home.css";

const Home = (props) => {
  const [contact, setContact] = useState(null);

  useEffect(() => {
    setContact(null);
    props.firebase.getCollection("Kontakt").then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if (doc.id === "Župa Presvetog Trojstva Blagaj Buna")
          setContact(doc.data());
      });
    });
  }, [props.firebase]);

  return (
    <div className="panel">
      <div className="panel-title">
        <h1>Župa Presvetog Trojstva Blagaj Buna</h1>
      </div>
      <hr />
      <div className="panel-body">
        <div className="panel-body-left home-container">
          {contact !== null && <div className="home-text">Kontakt podaci</div>}
          {contact !== null && (
            <>
              {contact.uris.map((url, key) => {
                return (
                  <div key={key} className="home-text">
                    {url.field}
                    {": "}
                    {url.scheme === "https" || url.scheme === "http" ? (
                      <a href={url.display}>{`${url.display}`}</a>
                    ) : (
                      url.display
                    )}
                  </div>
                );
              })}
              {contact.informacije.map((info, key) => {
                return (
                  <div key={key} className="home-text">
                    {info}
                  </div>
                );
              })}
            </>
          )}
        </div>
        <div className="panel-body-right">
          <img className="panel-photo" src={photo} alt="Crkva na Buni" />
          {props.firebase.auth.currentUser !== null && (
            <>
              <div className="home-text">Trenutno prijavljeni korisnik:</div>
              <div className="home-text">
                {props.firebase.auth.currentUser.email}
              </div>
              <div className="home-text">Dodajte novog korisnika</div>
              <button
                className="panel-button"
                onClick={() => props.history.push("/signup")}
              >
                Registriraj novog korisnika
              </button>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

const Homepage = withRouter(withFirebase(Home));

export default Homepage;
