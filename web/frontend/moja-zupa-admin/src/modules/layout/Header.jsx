import React from "react";
import { NavLink, withRouter } from "react-router-dom";
import { withFirebase } from "../firebase";

const HeaderBase = (props) => {
  return (
    <header>
      <NavLink exact to="/home">
        <h5>Moja Župa</h5>
      </NavLink>
      {props.firebase.auth.currentUser === null ? (
        <NavLink exact to="/login">
          <h5>Prijavi se</h5>
        </NavLink>
      ) : (
        <>
          <NavLink exact to="/sections">
            <h5>Sekcije</h5>
          </NavLink>
          <NavLink exact to="/cathecism">
            <h5>Vjeronauk</h5>
          </NavLink>
          <NavLink exact to="/hodocasce">
            <h5>Hodočašća</h5>
          </NavLink>
          <NavLink exact to="/obavijesti">
            <h5>Obavijesti</h5>
          </NavLink>
          <div
            style={{ cursor: "pointer" }}
            onClick={() => {
              props.firebase.signOut();
              props.history.push("/login");
            }}
          >
            <h5>Odjava</h5>
          </div>
        </>
      )}
    </header>
  );
};

const Header = withRouter(withFirebase(HeaderBase));

export { Header };
