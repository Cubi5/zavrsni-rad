import app from "firebase";
import "firebase/auth";

import { firebaseConfig } from "./config";

// Inspiration for this implementation of firebase in react is from https://www.robinwieruch.de/complete-firebase-authentication-react-tutorial
class Firebase {
  constructor() {
    app.initializeApp(firebaseConfig);

    this.auth = app.auth();
    this.db = app.firestore();
  }

  createUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  signInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  signOut = () => this.auth.signOut();

  passwordReset = (email) => this.auth.sendPasswordResetEmail(email);

  updatePassword = (password) => this.auth.currentUser.updatePassword(password);

  getCollection = (collection) => this.db.collection(collection).get();

  addToCollection = (collection, data) =>
    this.db
      .collection(collection)
      .doc(data.name)
      .set(data)
      .then(() => {
        console.log(`Data successfuly stored ${data}`);
      });

  deleteDocumentFromCollection = (collection, document) =>
    this.db
      .collection(collection)
      .doc(document)
      .delete()
      .then(() => {
        console.log(`Data successfuly deleted ${document}`);
      });

  getFirebaseTimestamp = (date) => app.firestore.Timestamp.fromDate(date);
}

export default Firebase;
