import { Header } from "./modules/layout/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { withAuthentication, LoginScreen, SignUpPage } from "./modules/session";
import HomePage from "./modules/pages/homePage";
import CategoryPage from "./modules/pages/categoryPage";
import Notifications from "./modules/pages/notifications";
import NotificationsPage from "./modules/pages/notificationsPage";
import HodocasniciPage from "./modules/pages/hodocasnici";

function App() {
  return (
    <div className="App">
      <Router>
        <Header></Header>
        <Switch>
          <Route exact path="/">
            <HomePage></HomePage>
          </Route>
          <Route exact path="/home">
            <HomePage></HomePage>
          </Route>
          <Route exact path="/login">
            <LoginScreen></LoginScreen>
          </Route>
          <Route exact path="/signup">
            <SignUpPage></SignUpPage>
          </Route>
          <Route exact path="/sections">
            <CategoryPage title="Sekcije" collection="Sekcije"></CategoryPage>
          </Route>
          <Route exact path="/cathecism">
            <CategoryPage
              title="Vjeronauk"
              collection="Vjeronauk"
            ></CategoryPage>
          </Route>
          <Route exact path="/hodocasce">
            <CategoryPage
              title="Hodočašća"
              collection="Hodočašća"
            ></CategoryPage>
          </Route>
          <Route exact path="/hodocasce/:name">
            <HodocasniciPage></HodocasniciPage>
          </Route>
          <Route exact path="/obavijesti">
            <Notifications
              title="Obavijesti"
              collection="Obavijesti"
            ></Notifications>
          </Route>
          <Route exact path="/obavijesti/:subCategory">
            <NotificationsPage
              title="Obavijesti"
              collection="Obavijesti"
            ></NotificationsPage>
          </Route>
          <Route path="*">
            <h1>Error 404 - Page not found</h1>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default withAuthentication(App);
