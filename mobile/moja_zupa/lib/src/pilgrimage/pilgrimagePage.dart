import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/category/category.dart';

class PilgrimagePage extends StatelessWidget {
  const PilgrimagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoryPage(
      "Hodočašća",
      "Hodočašća",
      isTrip: true,
    );
  }
}
