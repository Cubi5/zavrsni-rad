import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class PilgrimageForm extends StatefulWidget {
  const PilgrimageForm(this.name, {Key? key}) : super(key: key);

  final String name;

  @override
  _PilgrimageFormState createState() => _PilgrimageFormState();
}

class _PilgrimageFormState extends State<PilgrimageForm> {
  final _formKey = GlobalKey<FormState>();
  final firstnameEditor = TextEditingController();
  final lastnameEditor = TextEditingController();
  final fatherEditor = TextEditingController();

  @override
  void dispose() {
    firstnameEditor.dispose();
    lastnameEditor.dispose();
    fatherEditor.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Padding(
              padding: EdgeInsets.only(bottom: 15, top: 15),
              child: Text('Prijava za hodočašće: ' + widget.name,
                  style: Theme.of(context).textTheme.headline5),
            ),
          ),
          TextFormField(
            controller: firstnameEditor,
            decoration: InputDecoration(hintText: 'Ime'),
            textCapitalization: TextCapitalization.words,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Unesite ime';
              }
              return null;
            },
          ),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            controller: lastnameEditor,
            decoration: InputDecoration(hintText: 'Prezime'),
            textCapitalization: TextCapitalization.words,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Unesite prezime';
              }
              return null;
            },
          ),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            controller: fatherEditor,
            decoration: InputDecoration(hintText: 'Ime oca'),
            textCapitalization: TextCapitalization.words,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Unesite ime vašeg oca';
              }
              return null;
            },
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection('Hodočašća/${widget.name}/Hodocasnici')
                      .add({
                    "firstname": firstnameEditor.text,
                    "lastname": lastnameEditor.text,
                    "father": fatherEditor.text,
                    "payed": false,
                  });
                  Navigator.of(context).pop();
                },
                child: Text('Prijavi se'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Odustani'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
