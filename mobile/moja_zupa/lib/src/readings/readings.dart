import 'package:flutter/material.dart';
import 'package:moja_zupa/src/readings/reading.dart';

class ReadingsPage extends StatefulWidget {
  @override
  _ReadingsPageState createState() => _ReadingsPageState();
}

class _ReadingsPageState extends State<ReadingsPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Misna čitanja'),
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text('Nedjeljna'),
              ),
              Tab(
                child: Text('Svagdan'),
              ),
            ],
          ),
        ),
        body: TabBarView(children: [
          _buildSundayReadings(),
          _buildAllReadings(),
        ]),
      ),
    );
  }

  Widget _buildSundayReadings() {
    return ListView(
      children: [
        ReadingsListTile(
          title: 'Duhovi',
          onClick: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return ReadingPage(title: 'Duhovi');
            }),
          ),
          subtitle: '23.5.2021',
        ),
        ReadingsListTile(
            title: '6. vazmena nedjelja',
            onClick: () => {},
            subtitle: '16.5.2021'),
        ReadingsListTile(
            title: '5. vazmena nedjelja',
            onClick: () => {},
            subtitle: '9.5.2021'),
        ReadingsListTile(
            title: '4. vazmena nedjelja',
            onClick: () => {},
            subtitle: '2.5.2021'),
      ],
    );
  }

  Widget _buildAllReadings() {
    return ListView(
      children: [
        ReadingsListTile(
            title: 'Ponedjeljak - svagdan',
            onClick: () => {},
            subtitle: '24.5.2021'),
        ReadingsListTile(
            title: 'Subota - sv. Rita',
            onClick: () => {},
            subtitle: '22.5.2021'),
        ReadingsListTile(
            title: 'Petak - sv. Marko',
            onClick: () => {},
            subtitle: '21.5.2021'),
        ReadingsListTile(
            title: 'Četvrtak - svagdan',
            onClick: () => {},
            subtitle: '20.5.2021'),
      ],
    );
  }
}

class ReadingsListTile extends StatelessWidget {
  const ReadingsListTile(
      {required this.title, required this.onClick, required this.subtitle});
  final String title;
  final String subtitle;
  final void Function() onClick;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListTile(
        title: Text(
          title,
        ),
        subtitle: Text(
          subtitle,
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 2, horizontal: 12),
        onTap: () => onClick(),
      ),
      Divider(
        indent: 12,
        endIndent: 12,
      ),
    ]);
  }
}
