import 'package:flutter/material.dart';

class ReadingPage extends StatefulWidget {
  const ReadingPage({required this.title});
  final String title;

  @override
  _ReadingPageState createState() => _ReadingPageState();
}

class _ReadingPageState extends State<ReadingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ReadingWidget(),
    );
  }
}

class ReadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Prvo čitanje:',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Čitanje Djela apostolskih\n\nKad je napokon došao dan Pedesetnice, svi su bili zajedno na istome mjestu. I eto iznenada šuma s neba, kao kad se digne silan vjetar. Ispuni svu kuću u kojoj su bili. I pokažu im se kao neki ognjeni razdijeljeni jezici te siđe po jedan na svakoga od njih. Svi se napuniše Duha Svetoga i počeše govoriti drugim jezicima, kako im već Duh davaše zboriti. A u Jeruzalemu su boravili Židovi, ljudi pobožni iz svakog naroda pod nebom. Pa kad nasta ona huka, strča se mnoštvo i smete jer ih je svatko čuo govoriti svojim jezikom. Svi su bili izvan sebe i divili se govoreći: »Gle! Nisu li svi ovi što govore Galilejci? Pa kako to da ih svatko od nas čuje na svojem materinskom jeziku? Parti, Međani, Elamljani, žitelji Mezopotamije, Judeje i Kapadocije, Ponta i Azije, Frigije i Pamfilije, Egipta i krajeva libijskih oko Cirene, pridošlice Rimljani, Židovi i sljedbenici, Krećani i Arapi – svi ih mi čujemo gdje našim ¬jezicima razglašuju veličanstvena djela Božja.«\n\nRiječ Gospodnja.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Otpjevni psalam:',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Pripjev:',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Pošalji Duha svojega, Gospodine, i obnovi lice zemlje!',
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontStyle: FontStyle.italic),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Blagoslivljaj, dušo moja, Gospodina!\nGospodine, Bože moj silno si velik!\nKako su brojna tvoja djela, Gospodine,\npuna je zemlja stvorenja tvojih.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Ako dah im oduzmeš, ugibaju,\ni opet se u prah vraćaju.\nPošalješ li dah svoj, opet nastaju,\ni tako obnavljaš lice zemlje.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Neka dovijeka traje slava Gospodnja:\nnek se raduje Gospodin u djelima svojim!\nBilo mu milo pjevanje moje!\nJa ću se radovati u Gospodinu.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Drugo čitanje:',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Čitanje Poslanice svetoga Pavla apostola Galaćanima\n\nBraćo: Po Duhu živite pa nećete ugađati požudi tijela! Jer tijelo žudi protiv Duha, a Duh protiv tijela. Doista, to se jedno drugomu protivi da ne činite što hoćete. Ali ako vas Duh vodi, niste pod Zakonom.\nA očita su djela tijela. To su: bludnost, nečistoća, razvratnost, idolopoklonstvo, vračanje, neprijateljstva, svađa, ljubomor, srdžbe, spletkarenja, razdori, strančarenja, zavisti, pijančevanja, pijanke i tome slično. Unaprijed vam kažem, kao što vam već rekoh: koji takvo što čine, kraljevstva Božjega neće baštiniti. Plod je pak Duha: ljubav, radost, mir, velikodušnost, uslužnost, dobrota, vjernost, blagost, uzdržljivost. Protiv tih nema zakona. Koji su Kristovi, razapeše tijelo sa strastima i požudama. Ako živimo po Duhu, po Duhu se i ravnajmo!\n\nRiječ Gospodnja.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Evanđelje',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Čitanje svetog Evanđelja po Ivanu\n\nU ono vrijeme: Reče Isus svojim učenicima:\n»Kada dođe Branitelj koga ću vam poslati od Oca - Duh Istine koji od Oca izlazi - on će svjedočiti za mene. I vi ćete svjedočiti jer ste od početka sa mnom.\nJoš vam mnogo imam kazati, ali sada ne možete nositi. No kada dođe on - Duh Istine -upućivat će vas u svu istinu; jer neće govoriti sam od sebe, nego će govoriti što čuje i navješćivat će vam ono što dolazi. On će mene proslavljati jer će od mojega uzimati i navješćivati vama. Sve što ima Otac, moje je. Zbog toga vam rekoh: od mojega uzima i - navješćivat će vama."\n\nRiječ Gospodnja.',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      ],
    );
  }
}
