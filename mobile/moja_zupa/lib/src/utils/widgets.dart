import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

PreferredSize getTabBar(BuildContext context, List<String> tabsList) {
  TabBar tapBar = TabBar(
    tabs: tabsList.map((e) => Tab(child: Text(e))).toList(),
  );
  return PreferredSize(
    preferredSize: tapBar.preferredSize,
    child: Container(
      color: Theme.of(context).backgroundColor.withOpacity(0.5),
      child: tapBar,
    ),
  );
}

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SpinKitFadingCircle(
        color: Theme.of(context).primaryColor,
        size: 50,
      ),
    );
  }
}

class NotificationIcon extends StatefulWidget {
  NotificationIcon({Key? key, required this.topic, this.color})
      : super(key: key);

  final String topic;
  Color? color;

  @override
  _NotificationIconState createState() => _NotificationIconState();
}

class _NotificationIconState extends State<NotificationIcon> {
  var _bell = Icons.notifications_none;

  @override
  Widget build(BuildContext context) {
    var color =
        widget.color != null ? widget.color! : Theme.of(context).primaryColor;
    print(color);
    return FutureBuilder<SharedPreferences>(
      future: SharedPreferences.getInstance(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          bool isOn = snapshot.data!.getBool(widget.topic) ?? false;
          _bell = isOn ? Icons.notifications : Icons.notifications_none;
          return IconButton(
            icon: Icon(
              _bell,
              color: color,
              size: 30,
            ),
            onPressed: () => _onPressed(),
          );
        }

        return IconButton(
          icon: Icon(
            _bell,
            color: color,
            size: 30,
          ),
          onPressed: () => _onPressed(),
        );
      },
    );
  }

  void _onPressed() async {
    final prefs = await SharedPreferences.getInstance();
    bool isOn = prefs.getBool(widget.topic) ?? false;
    setState(() {
      _bell = isOn ? Icons.notifications : Icons.notifications_none;
    });
    prefs.setBool(widget.topic, !isOn);
    if (!isOn) {
      FirebaseMessaging.instance.subscribeToTopic(widget.topic);
    } else {
      FirebaseMessaging.instance.unsubscribeFromTopic(widget.topic);
    }
  }
}

class ColoredDivider extends StatelessWidget {
  const ColoredDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Theme.of(context).accentColor,
      indent: 12,
      endIndent: 12,
    );
  }
}

class SnapshotError extends StatelessWidget {
  const SnapshotError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Nešto je pošlo po krivu.' +
            ' Probajte ponovno pokrenuti aplikaciju.' +
            ' Ako se problem nastavi, molim Vas' +
            ' kontaktirajte razvojne inženjere.',
        style: TextStyle(color: Theme.of(context).errorColor),
      ),
    );
  }
}
