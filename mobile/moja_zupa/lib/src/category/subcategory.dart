import 'package:flutter/material.dart';
import 'package:moja_zupa/src/category/informationList.dart';
import 'package:moja_zupa/src/notification/notifications_list.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class SubCategoryPage extends StatefulWidget {
  const SubCategoryPage(
      {required this.name,
      required this.topic,
      required this.id,
      required this.collection});
  final String collection;
  final String name;
  final String topic;
  final String id;

  @override
  _SubCategoryPageState createState() => _SubCategoryPageState();
}

class _SubCategoryPageState extends State<SubCategoryPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.name),
          bottom: getTabBar(context, ['Obavijesti', 'Informacije']),
          actions: [
            NotificationIcon(
              topic: widget.topic,
              color: Theme.of(context).secondaryHeaderColor,
            ),
          ],
        ),
        body: TabBarView(
          children: [
            NotificationList(widget.id),
            InformationList(id: widget.name, collection: widget.collection),
          ],
        ),
      ),
    );
  }
}
