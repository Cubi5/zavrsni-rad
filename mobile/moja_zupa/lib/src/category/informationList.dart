import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class InformationList extends StatefulWidget {
  InformationList({required this.id, required this.collection});

  final String id;
  final String collection;
  @override
  _InformationListState createState() => _InformationListState();
}

class _InformationListState extends State<InformationList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
      stream: FirebaseFirestore.instance
          .collection(widget.collection)
          .doc(widget.id)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting ||
            !snapshot.hasData) {
          return Loading();
        }

        return new ListView(
          children: snapshot.data!.get('informacije').map<Widget>((info) {
            return Padding(
              padding: EdgeInsets.only(left: 25, top: 15),
              child: Text(info),
            );
          }).toList(),
        );
      },
    );
  }
}
