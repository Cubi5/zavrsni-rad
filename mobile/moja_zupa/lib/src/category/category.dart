import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/category/subcategory.dart';
import 'package:moja_zupa/src/pilgrimage/pilgrimageForm.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class CategoryPage extends StatefulWidget {
  CategoryPage(this.title, this.collection, {this.isTrip = false});

  final bool isTrip;
  final String title;
  final String collection;
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _buildList(context),
    );
  }

  Widget _buildList(context) {
    return StreamBuilder<QuerySnapshot>(
      stream:
          FirebaseFirestore.instance.collection(widget.collection).snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        if (snapshot.hasData) {
          return new ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              return new CategoryListTile(
                collection: widget.collection,
                name: document.get('name'),
                topic: document.get('topic'),
                id: document.id,
                isTrip: widget.isTrip,
              );
            }).toList(),
          );
        } else {
          return Center(child: new Text('Nema obavijesti'));
        }
      },
    );
  }
}

class CategoryListTile extends StatefulWidget {
  const CategoryListTile(
      {required this.name,
      required this.topic,
      required this.id,
      required this.collection,
      this.isTrip = false});
  final String collection;
  final String name;
  final String topic;
  final String id;
  final bool isTrip;

  @override
  _CategoryListTileState createState() => _CategoryListTileState();
}

class _CategoryListTileState extends State<CategoryListTile> {
  @override
  Widget build(BuildContext context) {
    var tap = () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return SubCategoryPage(
              collection: widget.collection,
              name: widget.name,
              topic: widget.topic,
              id: widget.id,
            );
          }),
        ).then((_) => {setState(() {})});

    return Column(children: [
      ListTile(
        title: Text(
          widget.name,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 2, horizontal: 12),
        trailing: widget.isTrip
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: Icon(Icons.add),
                    color: Theme.of(context).primaryColor,
                    iconSize: 30,
                    onPressed: () => {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [PilgrimageForm(widget.name)],
                          ),
                        ),
                      )
                    },
                  ),
                  NotificationIcon(topic: widget.topic),
                ],
              )
            : NotificationIcon(topic: widget.topic),
        onTap: tap,
      ),
      ColoredDivider(),
    ]);
  }
}
