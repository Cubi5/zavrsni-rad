import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/cathecism/cathecisms.dart';
import 'package:moja_zupa/src/contact/contact_page.dart';
import 'package:moja_zupa/src/pilgrimage/pilgrimagePage.dart';
import 'package:moja_zupa/src/notification/notifications_page.dart';
import 'package:moja_zupa/src/parish_paper/parish_paper.dart';
import 'package:moja_zupa/src/readings/readings.dart';
import 'package:moja_zupa/src/sections/sections.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Župa Presvetog Trojstva Blagaj Buna",
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 10),
        children: _buildGridList(),
      ),
    );
  }

  List<Widget> _buildGridList() {
    return [
      _buildGridListTile(
        'Obavijesti',
        Icons.notifications,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return NotificationsPage();
          }),
        ),
      ),
      _buildGridListTile(
        'Vjeronauk',
        Icons.groups_outlined,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return CathecismsPage();
          }),
        ),
      ),
      _buildGridListTile(
        'Sekcije',
        Icons.audiotrack_outlined,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return SectionPage();
            //return SectionsPage();
          }),
        ),
      ),
      _buildGridListTile(
        'Hodočašća',
        Icons.follow_the_signs,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return PilgrimagePage();
          }),
        ),
      ),
      _buildGridListTile(
        'Misna čitanja',
        Icons.book_outlined,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return ReadingsPage();
          }),
        ),
      ),
      _buildGridListTile(
        'Župni listovi',
        Icons.article_outlined,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return ParishPaperPage();
          }),
        ),
      ),
      _buildGridListTile(
        'Kontakt',
        Icons.contact_page_outlined,
        () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return ContactPage();
          }),
        ),
      ),
      _buildGridListTile('Postavke', Icons.settings, () => {}),
    ];
  }

  Widget _buildGridListTile(String title, IconData icon, Function onTap) {
    return Column(
      children: [
        ListTile(
          leading: Icon(
            icon,
            color: Theme.of(context).accentColor,
            size: 35,
          ),
          title: Text(
            title,
            style: Theme.of(context).textTheme.headline5,
          ),
          onTap: () => {onTap()},
        ),
        Divider(
          color: Theme.of(context).accentColor,
          indent: 12,
          endIndent: 12,
        ),
      ],
    );
  }
}
