import 'package:flutter/material.dart';
import 'package:moja_zupa/src/notification/topics_list.dart';
import 'package:moja_zupa/src/notification/topics_list_subscribed.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Obavijesti'),
          bottom: getTabBar(context, ['Pretplaćene', 'Sve']),
        ),
        body: TabBarView(children: [
          TopicsListSubscribed(),
          TopicsList(),
        ]),
      ),
    );
  }
}
