import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/notification/topics_list.dart';
import 'package:moja_zupa/src/utils/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TopicsListSubscribed extends StatefulWidget {
  @override
  _TopicsListSubscribedState createState() => _TopicsListSubscribedState();
}

class _TopicsListSubscribedState extends State<TopicsListSubscribed> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('Obavijesti').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        if (snapshot.hasData) {
          return FutureBuilder<SharedPreferences>(
            future: SharedPreferences.getInstance(),
            builder: (context, prefShot) {
              if (prefShot.hasData) {
                return ListView(
                  children: snapshot.data!.docs.where((d) {
                    if (prefShot.data!.getBool(d.get('topic')) == null) {
                      return false;
                    } else if (prefShot.data!.getBool(d.get('topic'))!) {
                      return true;
                    }

                    return false;
                  }).map((DocumentSnapshot document) {
                    return new TopicListTile(
                      title: document.get('name'),
                      topic: document.get('topic'),
                      id: document.id,
                    );
                  }).toList(),
                );
              }

              return Center(child: Text('Loading...'));
            },
          );
        } else {
          return new Text('Nema obavijesti');
        }
      },
    );
  }
}
