import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/notification/notifications_list.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class TopicsList extends StatefulWidget {
  @override
  _TopicsListState createState() => _TopicsListState();
}

class _TopicsListState extends State<TopicsList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('Obavijesti').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        if (snapshot.hasData) {
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              return new TopicListTile(
                title: document.get('name'),
                topic: document.get('topic'),
                id: document.id,
              );
            }).toList(),
          );
        } else {
          return new Text('Nema obavijesti');
        }
      },
    );
  }
}

class TopicListTile extends StatefulWidget {
  const TopicListTile(
      {required this.title, required this.topic, required this.id});
  final String title;
  final String topic;
  final String id;

  @override
  _TopicListTileState createState() => _TopicListTileState();
}

class _TopicListTileState extends State<TopicListTile> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListTile(
        title: Text(
          widget.title,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 2, horizontal: 12),
        trailing: NotificationIcon(topic: widget.topic),
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return NotificationPage(
              title: widget.title,
              topic: widget.topic,
              id: widget.id,
            );
          }),
        ).then((_) => {setState(() {})}),
      ),
      ColoredDivider()
    ]);
  }
}
