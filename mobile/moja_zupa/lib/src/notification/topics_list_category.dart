import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/notification/topics_list.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class TopicsListCategory extends StatefulWidget {
  TopicsListCategory(this.category);

  final String category;

  @override
  _TopicsListCategoryState createState() => _TopicsListCategoryState();
}

class _TopicsListCategoryState extends State<TopicsListCategory> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('Obavijesti')
          .where('category', isEqualTo: widget.category)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        if (snapshot.hasData) {
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              return new TopicListTile(
                title: document.get('name'),
                topic: document.get('topic'),
                id: document.id,
              );
            }).toList(),
          );
        } else {
          return new Text('Nema obavijesti');
        }
      },
    );
  }
}
