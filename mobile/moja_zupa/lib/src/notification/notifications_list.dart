import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moja_zupa/src/utils/widgets.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage(
      {required this.title, required this.topic, required this.id});
  final String title;
  final String topic;
  final String id;

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          NotificationIcon(
            topic: widget.topic,
            color: Theme.of(context).secondaryHeaderColor,
          ),
        ],
      ),
      body: NotificationList(widget.id),
    );
  }
}

class NotificationList extends StatefulWidget {
  NotificationList(this.id);

  final String id;
  @override
  _NotificationListState createState() => _NotificationListState();
}

class _NotificationListState extends State<NotificationList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('Obavijesti')
          .doc(widget.id)
          .collection('Obavijesti')
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        return new ListView(
          children: snapshot.data!.docs.map((doc) {
            return NotificationTile(doc.get('title'), doc.get('content'),
                doc.get('timestamp').toDate());
          }).toList(),
        );
      },
    );
  }
}

class NotificationTile extends StatelessWidget {
  const NotificationTile(this._title, this._content, this._timestamp);
  final String _title;
  final String _content;
  final DateTime _timestamp;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(
            _title,
            style: Theme.of(context).textTheme.bodyText2,
          ),
          trailing: Text(
            DateFormat('dd.MM.yyyy').format(_timestamp),
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(_content),
        ),
        ColoredDivider()
      ],
    );
  }
}
