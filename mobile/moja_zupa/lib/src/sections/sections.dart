import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/category/category.dart';

class SectionPage extends StatelessWidget {
  const SectionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoryPage("Sekcije", "Sekcije");
  }
}
