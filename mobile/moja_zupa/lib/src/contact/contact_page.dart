import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/utils/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactPage extends StatefulWidget {
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Kontakt'),
            bottom: getTabBar(context, ['O župi', 'Kontakt']),
          ),
          body: TabBarView(
            children: <Widget>[
              _getInfoWidget(),
              UrlWidget(),
            ],
          ),
        ));
  }

  Widget _getInfoWidget() {
    return StreamBuilder<DocumentSnapshot>(
      stream: FirebaseFirestore.instance
          .collection('Kontakt')
          .doc('Župa Presvetog Trojstva Blagaj Buna')
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        return new ListView(
          children: snapshot.data!.get('informacije').map<Widget>((info) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 25, top: 10, right: 25),
                  child:
                      Text(info, style: Theme.of(context).textTheme.bodyText2),
                ),
              ],
            );
          }).toList(),
        );
      },
    );
  }
}

class UrlWidget extends StatefulWidget {
  const UrlWidget({Key? key}) : super(key: key);

  @override
  _UrlWidgetState createState() => _UrlWidgetState();
}

class _UrlWidgetState extends State<UrlWidget> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: FirebaseFirestore.instance
          .collection('Kontakt')
          .doc('Župa Presvetog Trojstva Blagaj Buna')
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        return new ListView(
          children: snapshot.data!.get('uris').map<Widget>((uri) {
            final Uri launchUri = Uri(
              scheme: uri['scheme'],
              path: uri['path'],
            );
            return UrlTile(
                field: uri['field'],
                display: uri['display'],
                uri: launchUri,
                scheme: uri['scheme']);
          }).toList(),
        );
      },
    );
  }
}

class UrlTile extends StatelessWidget {
  const UrlTile(
      {Key? key,
      required this.display,
      required this.field,
      required this.uri,
      required this.scheme})
      : super(key: key);

  final String display;
  final String field;
  final String scheme;
  final Uri uri;

  @override
  Widget build(BuildContext context) {
    var icon;
    switch (scheme) {
      case 'tel':
        icon = Icons.phone;
        break;
      case 'mailto':
        icon = Icons.email;
        break;
      case 'https':
      case 'http':
        icon = Icons.link;
        break;
      default:
        icon = Icons.link;
    }

    return ListTile(
      title: Text(display, style: Theme.of(context).textTheme.bodyText2),
      subtitle: Text(
        field,
      ),
      leading: Icon(icon, size: 35, color: Theme.of(context).primaryColor),
    );
  }
}

void launchURL(Uri launchUri) async {
  if (await canLaunch(launchUri.toString())) {
    await launch(launchUri.toString());
  }
}
