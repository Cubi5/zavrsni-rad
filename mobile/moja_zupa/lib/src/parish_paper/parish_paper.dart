import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/utils/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class ParishPaperPage extends StatefulWidget {
  @override
  _ParishPaperPageState createState() => _ParishPaperPageState();
}

class _ParishPaperPageState extends State<ParishPaperPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Župni listovi'),
      ),
      body: _buildParishPaperList(),
    );
  }

  Widget _buildParishPaperList() {
    return StreamBuilder<QuerySnapshot>(
      stream:
          FirebaseFirestore.instance.collection('Župni listovi').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SnapshotError();
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Loading();
        }

        return new ListView(
          children: snapshot.data!.docs.map((doc) {
            return ParishListTile(
              title: doc.get('display'),
              uri: doc.get('url'),
            );
          }).toList(),
        );
      },
    );
  }
}

class ParishListTile extends StatelessWidget {
  const ParishListTile({required this.title, required this.uri});
  final String title;
  final String uri;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListTile(
        title: Text(
          title,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        onTap: () => _launchURL(uri),
        trailing: Icon(
          Icons.download,
          color: Theme.of(context).primaryColor,
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 2, horizontal: 12),
      ),
      ColoredDivider(),
    ]);
  }

  void _launchURL(uri) async {
    if (await canLaunch(uri)) {
      await launch(uri);
    }
  }
}
