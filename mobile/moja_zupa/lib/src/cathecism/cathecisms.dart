import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moja_zupa/src/category/category.dart';

class CathecismsPage extends StatelessWidget {
  const CathecismsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoryPage("Vjeronauk", "Vjeronauk");
  }
}
